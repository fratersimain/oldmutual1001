﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OM.GIS.TechnicalEvaluation.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NDesk.Options;
using Microsoft.Practices.Unity;

namespace OM.GIS.TechnicalEvaluation
{

    // Write an application to output the weather for a particular city in a particular country.
    // The application must be able to output the current weather, observation time, city and temperature
    // (in either celcius or fahrenheit), and wind speed in either miles per hour, or kilometers per second.

    // Please feel free to make use of any libraries available via NuGet

    // The application must use the Weather Underground web service. An example of the endpoint to use is -

    // http://api.wunderground.com/api/Your_Key/conditions/q/za/cape%20town.json

    // Examples:
    // --
    // omtech.exe -tempunit:C -dunit:km -country:ZA -city:Cape Town 
    // omtech.exe -tempunit:C -dunit:m -country:ZA -station:FACT

    // If units are not provided, default to configuration file values.

    // Should you wish to login to the wunderground site to review the api documentation, 
    // see http://www.wunderground.com/weather/api/d/docs

    // Username: m+omtechtest@praxion.co.za
    // Password: Y59&bqk!qfkGG9w*zbp2

    // You will need an API key for the Weather Underground service. This should be specified in the configuration 
    // file of the application. You can use the following API key: 244b4592ca93b308.

    // TODO: Parse command line arguments to set program options
    // TODO: Query the service with the requested parameters
    // TODO: Parse and deserialise the response
    // TODO: Output the data to stdout
    // Extra Credit: Support multiple cities in one call


    class Program
    {
        #region Dependencies
        private static ICommandLineParser _commandLineParser;
        private static IWeatherService _weatherService;
        #endregion

        static void Main(string[] args)
        {
            ///Wrap static main in async handler
            Task mainAsync = MainAsync(args);
            mainAsync.Wait();
        }

        static async Task MainAsync(string[] args)
        {
            ///Bootstrap Unity IOC
            IUnityContainer unitycontainer = new UnityContainer();
            unitycontainer.RegisterType<ICommandLineParser, CommandLineParser>();
            unitycontainer.RegisterType<IWeatherService, WeatherService>();
            unitycontainer.RegisterType<IAPIService, APIService>();

            ///Set dependencies (usually use IOC pattern in constructor with Unity)
            _commandLineParser = unitycontainer.Resolve<ICommandLineParser>();
            _weatherService = unitycontainer.Resolve<IWeatherService>(); 

            ///Parse command line parameters
            _commandLineParser.Init(args);

            ///Display welcome
            Console.WriteLine("********************************************************");
            Console.WriteLine("**         Welcome to the Weather Channel             **");
            Console.WriteLine("********************************************************");
            Console.WriteLine();

            ///Check for command line parser errors
            if (_commandLineParser.Error != null)
            {
                Console.WriteLine("********************************************************");
                Console.WriteLine("**            Command Line Args Error                 **");
                Console.WriteLine("********************************************************");
                Console.WriteLine(_commandLineParser.Error.Message);
            }

            ///Prepare weather service model
            WeatherServiceRequest weatherAPIRequest = new WeatherServiceRequest
            {
                TempUnit = _commandLineParser.Tempunit,
                Cities = _commandLineParser.Cities,
                Country = _commandLineParser.Country,
                WindspeedUnit = _commandLineParser.WindspeedUnit
            };

            ///Get weather results
            string weatherAPIResult = await _weatherService.GetCurrentWeather(weatherAPIRequest);

           

            ///Display results to console
            Console.WriteLine("********************************************************");
            Console.WriteLine("**                     Results                        **");
            Console.WriteLine("********************************************************");
            Console.WriteLine(weatherAPIResult);

            ///Say goodbye
            Console.WriteLine();
            Console.WriteLine("********************************************************");
            Console.WriteLine("**              Press any key to quit                 **");
            Console.WriteLine("********************************************************");
            Console.WriteLine();

            ///Pause to display results
            Console.ReadKey();
        }
    }
}
