﻿using NDesk.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
    public interface ICommandLineParser
    {
        #region Public Properties
        string Tempunit { get; set; }
        List<string> Cities { get; set; }
        string Country { get; set; }
        string WindspeedUnit { get; set; }
        OptionException Error { get; set; }
        #endregion

        #region Public Methods
        void Init(string[] args);
        #endregion
    }

    public class CommandLineParser : ICommandLineParser
    {
        #region Public Properties
        public string Tempunit { get; set; }
        public List<string> Cities { get; set; }
        public string Country { get; set; }
        public string WindspeedUnit { get; set; }
        public OptionException Error { get; set; }
        #endregion

        #region Public Methods
        public void Init(string[] args)
        {
            ///Set default values 
            this.Tempunit = "c";
            this.Cities = new List<string>();
            this.Country = "";
            this.WindspeedUnit = "km";

            ///Define Command Line Options
            OptionSet options = new OptionSet {
                            { "tempunit:", "The temperature unit to use. Either 'C' or 'F'.", v =>  this.Tempunit = v },
                            { "city:", "The city to use.", v =>  this.Cities.Add(v) },
                            { "country:", "The country to use.", v =>  this.Country = v },
                            { "dunit:", "The wind speed unit to use.  Either 'km' or 'm'.", v =>  this.WindspeedUnit = v },
                        };

            ///Parse Command Line Args
            List<string> extra;
            try
            {
                extra = options.Parse(args);
            }
            catch (OptionException e)
            {
                this.Error = e;
            }
        }
        #endregion
    }
}
