﻿using BizArk.Core;
using BizArk.Core.CmdLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
     class SimpleCommandLineArguments : CmdLineObject
    {
       

        [CmdLineArg(ShowInUsage = DefaultBoolean.True)]
        public string TempUnit { get; set; }

       [CmdLineArg(ShowInUsage = DefaultBoolean.True)]
        public string WindspeedUnit { get; set; }

       [CmdLineArg(ShowInUsage = DefaultBoolean.True)]
        public string Country { get; set; }

  [CmdLineArg(ShowInUsage = DefaultBoolean.True)]
        public string City { get; set; }

    }
}
