﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
    public class WeatherAPIResponse
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public JObject RawContent { get; set; }

        public string Weather { get; set; }
        public string ObservationTime { get; set; }
        public string City { get; set; }
        public string TemperatureC { get; set; }

        public string TemperatureF { get; set; }

        public string WindKPH { get; set; }

        public string WindMPH { get; set; }


        public Exception Error { get; set; }
    }
}
