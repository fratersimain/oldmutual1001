﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
    public class WeatherServiceRequest
    {
        #region Public Properties
       
        public string TempUnit { get; set; }

        public string WindspeedUnit { get; set; }

        public string Country { get; set; }

        public List<string> Cities { get; set; }

        #endregion
    }
}
