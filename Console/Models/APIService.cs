﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
    public interface IAPIService
    {

        Task<APIResponse> GetAsync(string baseAddress, string apiUrl);
    }

    class APIService : IAPIService
    {


        public async Task<APIResponse> GetAsync(string baseAddress, string apiUrl)
        {
            APIResponse result = new APIResponse();

            using (var client = new HttpClient())
            {
                ///Configure HTTP client
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (HttpResponseMessage response = await client.GetAsync(apiUrl))
                {
                    result.Content = await response.Content.ReadAsStringAsync();
                    result.HttpStatusCode = response.StatusCode;
                }

              
            }
            return result;
        }
    }
}
