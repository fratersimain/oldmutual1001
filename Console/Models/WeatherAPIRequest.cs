﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
    public class WeatherAPIRequest
    {
        public string TempUnit { get; set; }

        public string WindspeedUnit { get; set; }

        public string Country { get; set; }

        public string City { get; set; }
    }
}
