﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace OM.GIS.TechnicalEvaluation.Models
{
    public interface IWeatherService
    {
        Task<string> GetCurrentWeather(WeatherServiceRequest weatherServiceRequest);
    }

    public class WeatherService : IWeatherService
    {
        #region Dependencies
        IAPIService _apiService;
        #endregion

        #region Fields
        StringBuilder sb = new StringBuilder();
        #endregion

        #region Constructor
        public WeatherService(IAPIService apiService)
        {
            ///Set dependencies
            _apiService = apiService;
        }

        #endregion

        #region Private Methods
        private async Task<WeatherAPIResponse> GetAPIRequest(WeatherAPIRequest weatherAPIRequest)
        {
            ///Declare result
            WeatherAPIResponse result;

            ///Prepare API call
            string token = ConfigurationManager.AppSettings["WeatherAPIKey"];
            string baseAddress = ConfigurationManager.AppSettings["WeatherAPIBaseUrl"];

            string apiUrl = ConstructWeatherApiUrl(weatherAPIRequest, token);

            // HTTP GET
            try
            {
                APIResponse response = await _apiService.GetAsync(baseAddress, apiUrl);

                result = DeserialiseWeatherAPIResponse(response);
            }
            catch (Exception e)
            {
                result = new WeatherAPIResponse
                {
                    HttpStatusCode = System.Net.HttpStatusCode.ServiceUnavailable,
                    Error = e
                };
            }

            ///Return result
            return result;
        }

        private WeatherAPIResponse DeserialiseWeatherAPIResponse(APIResponse response)
        {
            ///Declare result 
            WeatherAPIResponse result = new WeatherAPIResponse();

            ///Get status code
            result.HttpStatusCode = response.HttpStatusCode;

            ///Get content
            if (response.HttpStatusCode == HttpStatusCode.OK)
            {
                ///Set results
                JObject content = JObject.Parse(response.Content);

                JToken currentObservation = content["current_observation"];
                JToken displayLocation = currentObservation["display_location"];
                JToken estimated = content["estimated"];
                result.RawContent = content;
                result.Weather = (string)currentObservation["weather"];
                result.TemperatureC = (string)currentObservation["temp_c"];
                result.TemperatureF = (string)currentObservation["temp_f"];

                result.WindKPH = (string)currentObservation["wind_kph"];
                result.WindMPH = (string)currentObservation["wind_mph"];
                result.City = (string)displayLocation["city"];
                result.ObservationTime = (string)currentObservation["observation_time"];
            }

            ///Return result
            return result;
        }

        private string ConstructWeatherApiUrl(WeatherAPIRequest weatherAPIRequest, string token)
        {
            string apiUrl = string.Format("{0}/conditions/q/{1}/{2}.json",
                token,
                weatherAPIRequest.Country,
                weatherAPIRequest.City);
            return apiUrl;
        }
        #endregion

        #region Public Methods
        public async Task<string> GetCurrentWeather(WeatherServiceRequest weatherServiceRequest)
        {
            ///Call Weather API for each city
            int cityCounter = 1;

            foreach (string city in weatherServiceRequest.Cities)
            {
                ///Prepare model for API call
                WeatherAPIRequest weatherAPIRequest = new WeatherAPIRequest
                {
                    City = city,
                    Country = weatherServiceRequest.Country,
                    TempUnit = weatherServiceRequest.TempUnit,
                    WindspeedUnit = weatherServiceRequest.WindspeedUnit
                };

                ///Call API
                WeatherAPIResponse result = await GetAPIRequest(weatherAPIRequest);

                ///Output city counter
                sb.AppendLine();
                sb.AppendLine(string.Format("Result:{0}", cityCounter.ToString()));

                ///Check for API error
                if (result.HttpStatusCode != System.Net.HttpStatusCode.OK)
                {
                    sb.AppendLine(string.Format("Weather API Status: \t{0}", result.HttpStatusCode));
                    sb.AppendLine(string.Format("Weather API Error: \t{0}", result.Error.Message));
                    continue;
                }

                ///Output results
                sb.AppendLine(string.Format("City: \t\t\t{0}", result.City));
                sb.AppendLine(string.Format("Weather: \t\t{0}", result.Weather));
                sb.AppendLine(string.Format("Observation Time: \t{0}", result.ObservationTime));

                ///Display temp in correct temp unit
                if (weatherServiceRequest.TempUnit.ToLower() == "c")
                {
                    sb.AppendLine(string.Format("Temperature C: \t\t{0}", result.TemperatureC));
                }
                else if (weatherServiceRequest.TempUnit.ToLower() == "f")
                {
                    sb.AppendLine(string.Format("Temperature F: \t\t{0}", result.TemperatureF));
                }
                else
                {
                    sb.AppendLine(string.Format("Invalid temperature unit: \t{0}", weatherServiceRequest.TempUnit));
                }

                ///Display wind speed in correct unit
                if (weatherServiceRequest.WindspeedUnit.ToLower() == "km")
                {
                    sb.AppendLine(string.Format("Wind speed km: \t\t{0}", result.WindKPH));
                }
                else if (weatherServiceRequest.WindspeedUnit.ToLower() == "m")
                {
                    sb.AppendLine(string.Format("Wind speed m: \t\t{0}", result.WindMPH));
                }
                else
                {
                    sb.AppendLine(string.Format("Invalid Wind speed unit: \t{0}", weatherServiceRequest.WindspeedUnit));
                }

                ///Increment city counter
                cityCounter++;
            }

            //Return result;
            return sb.ToString();
        }
        #endregion
    }
}
