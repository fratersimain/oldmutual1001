﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OM.GIS.TechnicalEvaluation.Models;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using OMTests.Fakes;

namespace OMTests
{
    [TestClass]
    public class ProgramUnitTests
    {
        IUnityContainer unitycontainer;

        public ProgramUnitTests()
        {
            ///Bootstrap Unity IOC
            unitycontainer = new UnityContainer();
            ///Use fakes
            unitycontainer.RegisterType<ICommandLineParser, FakeCommandLineParser>();
            unitycontainer.RegisterType<IAPIService, FakeAPIService>();

            ///Use real
            unitycontainer.RegisterType<IWeatherService, WeatherService>();
        }

        [TestMethod]
        public async Task WeatherServiceGetCurrentWeatherTest()
        {
            ///Arrange
            IWeatherService weatherService = unitycontainer.Resolve<IWeatherService>(); 
            
            WeatherServiceRequest weatherServiceRequest = new WeatherServiceRequest
            {
                Cities = new System.Collections.Generic.List<string> { 
                 "roger"
                },
                Country = "ZA",
                TempUnit = "f",
                WindspeedUnit = "m"
            };

            ///Act
            var result = await weatherService.GetCurrentWeather(weatherServiceRequest);

            ///Assert
            string correct = (@"
Result:1
City: 			Cape Town
Weather: 		Mostly Cloudy
Observation Time: 	Last Updated on September 17, 1:00 PM SAST
Temperature F: 		65.8
Wind speed m: 		7.7
");
            Assert.AreEqual(correct.Replace("  ", string.Empty), result.Replace("  ", string.Empty));
        }
    }
}
