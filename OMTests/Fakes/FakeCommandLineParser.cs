﻿using OM.GIS.TechnicalEvaluation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMTests.Fakes
{
    class FakeCommandLineParser : ICommandLineParser
    {
        public string Tempunit
        {
            get
            {
                return "f";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public List<string> Cities
        {
            get
            {
                return new List<string>(){
                    "Johannesburg", "Cape Town"
                };
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Country
        {
            get
            {
                return "ZA";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string WindspeedUnit
        {
            get
            {
                return "m";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public NDesk.Options.OptionException Error
        {
            get
            {
                return null;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Init(string[] args)
        {
            ///Do nothing
        }
    }
}
