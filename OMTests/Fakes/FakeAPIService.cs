﻿using OM.GIS.TechnicalEvaluation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OMTests.Fakes
{
    class FakeAPIService : IAPIService
    {
        public Task<APIResponse> GetAsync(string baseAddress, string apiUrl)
        {
            APIResponse result = new APIResponse{
             Content = @"
                        {
                          'response': {
                          'version':'0.1',
                          'termsofService':'http://www.wunderground.com/weather/api/d/terms.html',
                          'features': {
                          'conditions': 1
                          }
	                        }
                          ,	'current_observation': {
		                        'image': {
		                        'url':'http://icons.wxug.com/graphics/wu2/logo_130x80.png',
		                        'title':'Weather Underground',
		                        'link':'http://www.wunderground.com'
		                        },
		                        'display_location': {
		                        'full':'Cape Town, South Africa',
		                        'city':'Cape Town',
		                        'state':'',
		                        'state_name':'South Africa',
		                        'country':'ZA',
		                        'country_iso3166':'ZA',
		                        'zip':'00000',
		                        'magic':'1',
		                        'wmo':'68816',
		                        'latitude':'-33.97999954',
		                        'longitude':'18.60000038',
		                        'elevation':'42.00000000'
		                        },
		                        'observation_location': {
		                        'full':'St Duma, Kuilsriver, Cape Town, WESTERN CAPE',
		                        'city':'St Duma, Kuilsriver, Cape Town',
		                        'state':'WESTERN CAPE',
		                        'country':'SOUTH AFRICA',
		                        'country_iso3166':'ZA',
		                        'latitude':'-33.940739',
		                        'longitude':'18.692310',
		                        'elevation':'236 ft'
		                        },
		                        'estimated': {
		                        },
		                        'station_id':'IWESTERN332',
		                        'observation_time':'Last Updated on September 17, 1:00 PM SAST',
		                        'observation_time_rfc822':'Wed, 17 Sep 2014 13:00:02 +0200',
		                        'observation_epoch':'1410951602',
		                        'local_time_rfc822':'Wed, 17 Sep 2014 13:01:21 +0200',
		                        'local_epoch':'1410951681',
		                        'local_tz_short':'SAST',
		                        'local_tz_long':'Africa/Johannesburg',
		                        'local_tz_offset':'+0200',
		                        'weather':'Mostly Cloudy',
		                        'temperature_string':'65.8 F (18.8 C)',
		                        'temp_f':65.8,
		                        'temp_c':18.8,
		                        'relative_humidity':'75%',
		                        'wind_string':'From the NW at 7.7 MPH Gusting to 15.9 MPH',
		                        'wind_dir':'NW',
		                        'wind_degrees':319,
		                        'wind_mph':7.7,
		                        'wind_gust_mph':'15.9',
		                        'wind_kph':12.4,
		                        'wind_gust_kph':'25.6',
		                        'pressure_mb':'1011',
		                        'pressure_in':'29.86',
		                        'pressure_trend':'0',
		                        'dewpoint_string':'58 F (14 C)',
		                        'dewpoint_f':58,
		                        'dewpoint_c':14,
		                        'heat_index_string':'NA',
		                        'heat_index_f':'NA',
		                        'heat_index_c':'NA',
		                        'windchill_string':'NA',
		                        'windchill_f':'NA',
		                        'windchill_c':'NA',
		                        'feelslike_string':'65.8 F (18.8 C)',
		                        'feelslike_f':'65.8',
		                        'feelslike_c':'18.8',
		                        'visibility_mi':'6.2',
		                        'visibility_km':'10.0',
		                        'solarradiation':'--',
		                        'UV':'5','precip_1hr_string':'0.00 in ( 0 mm)',
		                        'precip_1hr_in':'0.00',
		                        'precip_1hr_metric':' 0',
		                        'precip_today_string':'0.00 in (0 mm)',
		                        'precip_today_in':'0.00',
		                        'precip_today_metric':'0',
		                        'icon':'mostlycloudy',
		                        'icon_url':'http://icons.wxug.com/i/c/k/mostlycloudy.gif',
		                        'forecast_url':'http://www.wunderground.com/global/stations/68816.html',
		                        'history_url':'http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=IWESTERN332',
		                        'ob_url':'http://www.wunderground.com/cgi-bin/findweather/getForecast?query=-33.940739,18.692310',
		                        'nowcast':''
	                        }
                        }
                ",
  HttpStatusCode = HttpStatusCode.OK
            };

            return  Task.Run(()=> result);
        }
    }
}
